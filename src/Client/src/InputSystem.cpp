#include "InputSystem.h"
#include "Services.h"

InputSystem::InputSystem()
:mIsInitialized(false)
{
}


InputSystem::~InputSystem()
{
	cleanup();
}

bool InputSystem::init()
{
	if (mIsInitialized)
		return false;

	mIsInitialized = true;
	return true;
}

void InputSystem::cleanup()
{
	if (mIsInitialized)
		mIsInitialized = false;
}

void InputSystem::pollInput()
{
	sf::Event inputEvent;

	while (Services::getGraphicsSystem()->getWindow()->pollEvent(inputEvent))
	{
		switch (inputEvent.type)
		{
		case sf::Event::Closed: // Window closed
			Services::getGraphicsSystem()->getWindow()->close();
			break;
		case sf::Event::KeyPressed:
			if (inputEvent.key.code == Input::Key::ESCAPE)
			{
				Services::getEventSystem()->fireEvent(Event(EventType::GAME_EXIT));
			}
			else if (inputEvent.key.code == Input::Key::DOWN_ARROW)
			{
				Services::getEventSystem()->fireEvent(Event(EventType::MOVE_DOWN));
			}
			else if (inputEvent.key.code == Input::Key::UP_ARROW)
			{
				Services::getEventSystem()->fireEvent(Event(EventType::MOVE_UP));
			}
			break;
		case sf::Event::KeyReleased:
			{
			if (inputEvent.key.code == Input::Key::DOWN_ARROW)
			{
				Services::getEventSystem()->fireEvent(Event(EventType::STOP_DOWN));
			}
			else if (inputEvent.key.code == Input::Key::UP_ARROW)
			{
				Services::getEventSystem()->fireEvent(Event(EventType::STOP_UP));
			}
			}
			break;
		default:
			break;
		}
	}
}