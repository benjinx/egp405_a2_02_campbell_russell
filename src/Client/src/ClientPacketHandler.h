#ifndef CLIENT_PACKET_HANDLER_H
#define CLIENT_PACKET_HANDLER_H
#include "../../common/src/PacketHandler.h"
#include "../../common/src/GamePacketHandler.h"
#include "RakPeerInterface.h"
#include <algorithm>
#include "Event.h"
#include "EventListener.h"
#include "EventSystem.h"



class PacketHandlerClient : public GamePacketHandler_I
{
public:
	PacketHandlerClient(rak_peer_ptr aPeer)
		: GamePacketHandler_I(aPeer)
	{ }

	bool HandleMessage(packet_id_type aID, rak_peer_ptr aPeer, packet_ptr aPacket);

private:
	bool mPosSet = false;
};

#endif CLIENT_PACKET_HANDLER_H