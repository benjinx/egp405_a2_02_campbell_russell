#include "Client.h"
#include "Game.h"
#include "Services.h"
#include "SFML/Window.hpp"

Client::Client(port_type aPort)
: EventListener()
, mPort(aPort)
{
	mPeer = RakNet::RakPeerInterface::GetInstance();
	mpPacketHandler = new PacketHandlerClient(mPeer);
	Services::getEventSystem()->addListener(EventType::MOVE_UP, this);
	Services::getEventSystem()->addListener(EventType::MOVE_DOWN, this);
	Services::getEventSystem()->addListener(EventType::STOP_UP, this);
	Services::getEventSystem()->addListener(EventType::STOP_DOWN, this);
	Services::getEventSystem()->addListener(EventType::RIGHT_SIDE, this);
	Services::getEventSystem()->addListener(EventType::LEFT_SIDE, this);
	Services::getEventSystem()->addListener(EventType::GAME_START, this);
	setRemoveOnDestruct(false); // Client shouldn't auto remove itself from the event system, that would be bad.
	
	// My shape info
	mMyShape.setSize(sf::Vector2f(20, 80));
	mMyShape.setPosition(100, 270 - (mMyShape.getSize().y / 2));
	mMyShape.setFillColor(sf::Color(100, 250, 50));
	mMyShape.setOutlineColor(sf::Color::White);
	mMyShape.setOutlineThickness(0.5f);

	mMyShapeVelocity = sf::Vector2f();

	// Other paddle velocity
	mOtherShapeVelocity = sf::Vector2f();

	// SET BALL INFO
	mBallShape.setSize(sf::Vector2f(20, 20));
	mBallShape.setPosition(480 - (mBallShape.getSize().x / 2), 270 - (mBallShape.getSize().y /2));
	
	mBallDirection.x = -3.0f;
	mBallDirection.y = 1.0f;


	// Populate bricks on left
	for (int i = 0; i < 24; ++i)
	{
		mBricks.push_back(sf::RectangleShape());
		mBricks[i].setFillColor(sf::Color::Blue);
		mBricks[i].setSize(sf::Vector2f(11.0f, 36.0f));
		mBricks[i].setPosition(sf::Vector2f((float)(20 + (40 * (i % 2))), (float)(30 + (40 * (i / 2)))));
		mBricks[i].setOutlineColor(sf::Color::White);
		mBricks[i].setOutlineThickness(0.5f);
	}

	// Populate bricks on right
	for (int i = 24; i < 48; ++i)
	{
		mBricks.push_back(sf::RectangleShape());
		mBricks[i].setFillColor(sf::Color::Red);
		mBricks[i].setSize(sf::Vector2f(11.0f, 36.0f));
		mBricks[i].setPosition(sf::Vector2f((float)(890 + (40 * ((i - 24) % 2))), (float)(30 + (40 * ((i - 24) / 2)))));
		mBricks[i].setOutlineColor(sf::Color::White);
		mBricks[i].setOutlineThickness(0.5f);
	}

	mGameStarted = false;
}

Client::~Client()
{
	delete mpPacketHandler;
	mpPacketHandler = nullptr;

	mPeer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(mPeer);
}

bool Client::Connect(const char* aServerAddress, port_type aServerPort) const
{
	// IPV4 socket
	RakNet::SocketDescriptor sd(mPort, 0);
	sd.socketFamily = AF_INET;

	mPeer->Startup(8, &sd, 1);
	mPeer->SetOccasionalPing(true);

	if (mPeer->Connect(aServerAddress, aServerPort, 0, 0) != RakNet::CONNECTION_ATTEMPT_STARTED)
	{
		printf("Attempt to connect to server FAILED.");
		return false;
	}

	printf("\nClient IP Address: ");
	for (unsigned int i = 0; i < mPeer->GetNumberOfAddresses(); i++)
		printf("%i. %s\n", i+1, mPeer->GetLocalIP(i));

	return true;
}

void Client::doNetworking(secType aDeltaT)
{
	if (mpPacketHandler)
		mpPacketHandler->update(aDeltaT);

	// Send our shape info.
	ShapeInfo si;
	si.mPacketID = packet_id::ID_PLAYER_STATE;
	si.mX = mMyShape.getPosition().x;
	si.mY = mMyShape.getPosition().y;
	si.mWidth = mMyShape.getSize().x;
	si.mHeight = mMyShape.getSize().y;
	si.mVelocity = mMyShapeVelocity;
	si.mGUID = getPeer()->GetMyGUID();
	getPeer()->Send((const char*)&si, sizeof(ShapeInfo), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);

	// Other shapes
	while (mpPacketHandler->getPlayerInfo().size() > mOtherShapes.size())
	{
		sf::RectangleShape newShape;
		newShape.setSize(sf::Vector2f(20, 80));
		newShape.setFillColor(sf::Color(sf::Color::Blue));
		newShape.setOutlineColor(sf::Color::White);
		newShape.setOutlineThickness(0.5f);
		mOtherShapes.push_back(newShape);

		mOtherShapeVelocity = mpPacketHandler->getPlayerInfo()[0].mVelocity;
	}
}

void Client::preUpdate()
{
	if (mGameStarted)
	{
		// Check for ball on screen and respawn otherwise
		if (mBallShape.getPosition().x >= Services::getGraphicsSystem()->getWidth() ||
			mBallShape.getPosition().y >= Services::getGraphicsSystem()->getHeight() ||
			mBallShape.getPosition().x <= 0 ||
			mBallShape.getPosition().y <= 0)
		{
			mBallShape.setPosition(480 - (mBallShape.getSize().x / 2), 270 - (mBallShape.getSize().y / 2));
			mBallDirection.x *= -1;
			mBallDirection.y = 2.0f - (((float)(rand() % 400)) / 100.0f);

			// Reset speed
			if (mBallDirection.x < 0)
				mBallDirection.x = -3.0f;
			else
				mBallDirection.x = 3.0f;
		}
			
		// Update the ball
		mBallShape.setPosition(mBallShape.getPosition().x + mBallDirection.x, mBallShape.getPosition().y + mBallDirection.y);

		// Update Paddles
		mMyShape.setPosition(mMyShape.getPosition().x, mMyShape.getPosition().y + mMyShapeVelocity.y);
		mOtherShapes[0].setPosition(mOtherShapes[0].getPosition().x, mOtherShapes[0].getPosition().y + mOtherShapeVelocity.y * 20.0f);
		
		// My paddle back in bounds
		if (mMyShape.getPosition().y <= 30)
		{
			mMyShapeVelocity.y = 0.0f;
			mMyShape.setPosition(mMyShape.getPosition().x, 30);
		}
			
		if (mMyShape.getPosition().y >= 430)
		{
			mMyShapeVelocity.y = 0.0f;
			mMyShape.setPosition(mMyShape.getPosition().x, 430);
		}
		
		// Enemy Paddle back in bounds
		if (mOtherShapes[0].getPosition().y <= 30)
		{
			mOtherShapeVelocity.y = 0.0f;
			mOtherShapes[0].setPosition(mOtherShapes[0].getPosition().x, 30);
		}

		if (mOtherShapes[0].getPosition().y >= 430)
		{
			mOtherShapeVelocity.y = 0.0f;
			mOtherShapes[0].setPosition(mOtherShapes[0].getPosition().x, 430);
		}
		

		// Checks to change the ball direction.
		if (mBallShape.getPosition().y + mBallShape.getSize().y >= Game::getInstance()->getBottomWall().getPosition().y) // if it touches the bottom wall.
		{
			mBallDirection.y *= -1;
		}

		if (mBallShape.getPosition().y <= Game::getInstance()->getTopWall().getPosition().y + Game::getInstance()->getTopWall().getSize().y) // if it touches the top wall.
		{
			mBallDirection.y *= -1;
		}

		// My paddle
		if (mBallShape.getPosition().x + mBallShape.getSize().x >= mMyShape.getPosition().x &&
			mBallShape.getPosition().x <= mMyShape.getPosition().x + mMyShape.getSize().x &&
			mBallShape.getPosition().y + mBallShape.getSize().x >= mMyShape.getPosition().y &&
			mBallShape.getPosition().y <= mMyShape.getPosition().y + mMyShape.getSize().y) 
		{ 
			mBallDirection.x *= -1;

			if (mBallDirection.x < 15 && mBallDirection.x > -15)
			mBallDirection.x *= 1.15f;

			// Use new direction to exit paddle x-wise
			while (mBallShape.getPosition().x + mBallShape.getSize().x >= mMyShape.getPosition().x &&
				mBallShape.getPosition().x + mBallShape.getSize().x <= mMyShape.getPosition().x + mMyShape.getSize().x)
			{
				mBallShape.setPosition(mBallShape.getPosition().x + mBallDirection.x, mBallShape.getPosition().y);
			}
		}

		// Opponent's paddle
		for (unsigned int i = 0; i < mpPacketHandler->getPlayerInfo().size(); i++)
		{
			if (mBallShape.getPosition().x + mBallShape.getSize().x >= mOtherShapes[i].getPosition().x &&
				mBallShape.getPosition().x <= mOtherShapes[i].getPosition().x + mOtherShapes[i].getSize().x &&
				mBallShape.getPosition().y + mBallShape.getSize().x >= mOtherShapes[i].getPosition().y &&
				mBallShape.getPosition().y <= mOtherShapes[i].getPosition().y + mOtherShapes[i].getSize().y)
			{
				mBallDirection.x *= -1;

				if (mBallDirection.x < 15 && mBallDirection.x > -15)
				mBallDirection.x *= 1.15f;

				// Use new direction to exit paddle x-wise
				while (mBallShape.getPosition().x + mBallShape.getSize().x >= mOtherShapes[i].getPosition().x &&
					mBallShape.getPosition().x + mBallShape.getSize().x <= mOtherShapes[i].getPosition().x + mOtherShapes[i].getSize().x)
				{
					mBallShape.setPosition(mBallShape.getPosition().x + mBallDirection.x, mBallShape.getPosition().y);
				}
			}
		}

		
		// Brick collisions
		for (unsigned int i = 0; i < mBricks.size(); ++i)
		{
			if (mBricks[i].getFillColor() != sf::Color::Black &&
				mBallShape.getPosition().x + mBallShape.getSize().x >= mBricks[i].getPosition().x &&
				mBallShape.getPosition().x <= mBricks[i].getPosition().x + mBricks[i].getSize().x &&
				mBallShape.getPosition().y + mBallShape.getSize().y >= mBricks[i].getPosition().y &&
				mBallShape.getPosition().y <= mBricks[i].getPosition().y + mBricks[i].getSize().y)
			{
				mBricks[i].setFillColor(sf::Color::Black);

				// Move back towards other side
				if (mBallShape.getPosition().x < 500)
				{
					// Reverse direction if going wrong direction x-wise
					if (mBallDirection.x < 0)
						mBallDirection.x *= -1;
				}
				else
				{
					if (mBallDirection.x > 0)
						mBallDirection.x *= -1;
				}
			}
		}
	}
}

void Client::postUpdate()
{
	// Draw mMyShape
	Services::getGraphicsSystem()->drawShape(mMyShape);

	// Draw opponent
	for (unsigned int i = 0; i < mpPacketHandler->getPlayerInfo().size(); i++)
	{
		mOtherShapes[i].setPosition(mpPacketHandler->getPlayerInfoAt(i).mState.mX, mpPacketHandler->getPlayerInfoAt(i).mState.mY);
		Services::getGraphicsSystem()->drawShape(mOtherShapes[i]);
	}

	// Draw Bricks
	for (unsigned int i = 0; i < mBricks.size(); ++i)
	{
		if (mBricks[i].getFillColor() != sf::Color::Black)
			Services::getGraphicsSystem()->drawShape(mBricks[i]);
	}

	// Draw ball
	Services::getGraphicsSystem()->drawShape(mBallShape);
}

void Client::handleEvent(const Event& theEvent)
{
	if (theEvent.getType() == EventType::MOVE_UP)
	{
		//if (mMyShape.getPosition().y >= 30)
			mMyShapeVelocity.y = -4;
	}
	else if (theEvent.getType() == EventType::MOVE_DOWN)
	{
		//if (mMyShape.getPosition().y <= 430)
			mMyShapeVelocity.y = 4;
	}
	else if (theEvent.getType() == EventType::STOP_UP)
	{
		if (mMyShapeVelocity.y < 0)
			mMyShapeVelocity.y = 0;
	}
	else if (theEvent.getType() == EventType::STOP_DOWN)
	{
		if (mMyShapeVelocity.y > 0)
			mMyShapeVelocity.y = 0;
	}
	else if (theEvent.getType() == EventType::RIGHT_SIDE)
	{
		printf("\nRight");
		mMyShape.setPosition(mMyShape.getPosition().x + 740, mMyShape.getPosition().y);
	}
	else if (theEvent.getType() == EventType::LEFT_SIDE)
	{
		printf("\nLeft");
		mMyShape.setPosition(mMyShape.getPosition().x, mMyShape.getPosition().y);
	}
	else if (theEvent.getType() == EventType::GAME_START) // Start moving the ball here.. or maybe call a bool and set to true and have an update elsewhere
	{
		printf("START!");
		if (!mGameStarted)
		{
			printf("Sent");
			GameStarting gs;
			gs.mPacketID = packet_id::ID_START_GAME;
			gs.gameStarting = true;
			gs.mGUID = getPeer()->GetMyGUID();
			getPeer()->Send((const char*)&gs, sizeof(GameStarting), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
		}
		mGameStarted = true;
		mBallShape.setPosition(mBallShape.getPosition().x, mBallShape.getPosition().y);
	}
}

void Client::printUsage()
{
	printf("\n-- USAGE --");
	printf("\nClient <serverIP> <serverPort> <clientPort>\n");
}

void Client::startingGame(bool isStarting)
{

}