#include "Game.h"
#include "GraphicsSystem.h"
#include "InputSystem.h"
#include "Services.h"
#include "Client.h"

Game* Game::mspGameInstance = NULL;

Game::Game()
	:EventListener()
	,mIsInitialized(false)
	,mIsGameRunning(false)
{
	mEventSystem.addListener(EventType::GAME_EXIT, this);

	setRemoveOnDestruct(false); // Game shouldn't auto remove itself from the event system.
}


Game::~Game()
{
	cleanup();
}


bool Game::init()
{
	if (mIsInitialized)
		return false;

	// Set width and height here.
	int displayWidth = 960, displayHeight = 540;

	// Initalize the systems.
	if (!mGraphicsSystem.init("My Game", displayWidth, displayHeight))
		return false;
	if (!mInputSystem.init())
		return false;

	// Provide systems to services
	Services::init();
	Services::provideGraphicsSystem(&mGraphicsSystem);
	Services::provideInputSystem(&mInputSystem);
	Services::provideEventSystem(&mEventSystem);

	// Timing
	mCurrentTime = sf::seconds(0.0f);
	mPreviousTime = sf::seconds(0.0f);
	mFpsUpdateTime = sf::seconds(0.0f);
	mEndFrameTime = sf::seconds(0.0f);
	mFrameLength = sf::seconds(1.0f / 60.0f);
	everyOtherFrame = true;

	// Load Assets
	const std::string PATH = "../Assets/";

	mpBackgroundTexture = new sf::Texture;
	mpBackgroundTexture->loadFromFile(PATH + "PongBackground.png");

	mpBackgroundSprite = new sf::Sprite();
	mpBackgroundSprite->setTexture(*mpBackgroundTexture);
	mpBackgroundSprite->setPosition(0, 0);
	
	mIsInitialized = true;
	return true;
}

void Game::cleanup()
{
	if (mIsInitialized)
	{
		// Shutdown systems.
		mGraphicsSystem.cleanup();
		mInputSystem.cleanup();
		Services::cleanup();

		// Release assets
		delete mpBackgroundSprite;
		delete mpBackgroundTexture;

		// Not initialized anymore.
		mIsInitialized = false;
	}
}

void Game::gameLoop(Client* apClient)
{
	// Game is running... so true
	mIsGameRunning = true;

	// Create clock and set fps.
	sf::Clock frameClock;
	mCurrentFps = 60;

	// Main Game loop.
	while (mGraphicsSystem.getWindow()->isOpen() && mIsGameRunning)
	{ 
		mCurrentTime = frameClock.getElapsedTime();
		mDeltaTime = mCurrentTime - mPreviousTime;

		// FrameRate
		mFpsUpdateTime += mDeltaTime;

		if (mFpsUpdateTime >= sf::seconds(1.0f))
		{
			mFpsUpdateTime -= sf::seconds(1.0f);
			mCurrentFps = mFrameCounter;
			mFrameCounter = 0;

			if (mCurrentFps < 0 || mCurrentFps > 60)
				mCurrentFps = 60;
		}


		// Handle the packets every other frame. 60 / 2 = 30.
		if (everyOtherFrame)
		{
			everyOtherFrame = false;

			sf::Clock clock;
			secType aDeltaT = clock.restart();
			apClient->doNetworking(aDeltaT);
		}
		else
		{
			everyOtherFrame = true;
		}
		
		// Client Update
		apClient->preUpdate();

		// Get Input
		mInputSystem.pollInput();

		// Update
		update(mDeltaTime);

		// Draw
		draw();

		// Draw Networked objects
		apClient->postUpdate();

		// Flip Display
		mGraphicsSystem.flipDisplay();

		// FrameRate
		++mFrameCounter;

		mPreviousTime = mCurrentTime;

		//-------Sleep Until Next Frame-------//
		mEndFrameTime = frameClock.getElapsedTime();
		if (mEndFrameTime - mCurrentTime < mFrameLength)
		{
			sf::sleep((mCurrentTime + mFrameLength) - mEndFrameTime);
		}

	}
}

void Game::update(sf::Time deltaTime)
{
	
}

void Game::draw()
{
	// Draw background
	mGraphicsSystem.drawSprite(*mpBackgroundSprite, sf::Rect<int>(sf::Vector2<int>(0, 0), sf::Vector2<int>(960, 540)), false);

	// Draw the walls
	mTopWall.setSize(sf::Vector2f(760, 10));
	mTopWall.setPosition(sf::Vector2f(100, 20));
	mGraphicsSystem.drawShape(mTopWall);

	mBottomWall.setSize(sf::Vector2f(760, 10));
	mBottomWall.setPosition(sf::Vector2f(100, 510));
	mGraphicsSystem.drawShape(mBottomWall);
}

void Game::endGame()
{
	mIsGameRunning = false;
}

void Game::handleEvent(const Event& theEvent)
{
	if (theEvent.getType() == EventType::GAME_EXIT)
		endGame();
}