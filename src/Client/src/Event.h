#ifndef EVENT_H
#define EVENT_H

#include <string>


enum EventType
{
	INVALID_EVENT_TYPE = -1,

	// Game
	MOVE_UP,			// Move Right
	MOVE_DOWN,	// Move left
	STOP_UP,
	STOP_DOWN,
	DEBUG,				// Debug menu
	GAME_EXIT,			// Exit the game.
	GAME_START,
	RIGHT_SIDE,
	LEFT_SIDE,

	NUM_EVENT_TYPES
};


class Event
{
public:
	Event(EventType type) :
		mType(type)
	{

	}
	virtual ~Event() {}

	EventType getType() const { return mType; };

private:
	EventType mType;
};


#endif // EVENT_H