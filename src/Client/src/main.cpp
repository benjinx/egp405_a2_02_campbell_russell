#include "Client.h"
#include <memory>
#include "Game.h"

int main(int argc, char** argv)
{
	// Seed rand
	srand(unsigned(time(NULL)));

	if (argc == 3 || argc == 4) // ServerIP serverport [clientPort]
	{

		Game::initInstance();
		if (!Game::getInstance()->init())
		{
			Game::cleanupInstance();
			return EXIT_FAILURE;
		}

		const char* serverIP = argv[1];
		const int serverPort = atoi(argv[2]);
		const int clientPort = argc == 4 ? atoi(argv[3]) : serverPort + 1;

		std::unique_ptr<Client> pClient(new Client(clientPort));
		pClient->Connect(serverIP, serverPort);

		Game::getInstance()->gameLoop(pClient.get());

		Game::getInstance()->cleanup();
		Game::cleanupInstance();
	}
	else
		Client::printUsage();

	system("pause");
	return EXIT_SUCCESS;
}