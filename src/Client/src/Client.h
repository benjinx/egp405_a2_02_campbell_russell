#ifndef CLIENT_H
#define CLIENT_H

#include <iostream>
#include <algorithm>
#include <memory>
#include "../../common/src/PacketHandler.h"
#include "../../common/src/GamePacketHandler.h"
#include "RakPeerInterface.h"
#include "SFML/System/Clock.hpp"
#include "SFML/Graphics.hpp"
#include "Event.h"
#include "EventListener.h"
#include "ClientPacketHandler.h"

namespace RakNet { class RakPeerInterface; };

class Client : public EventListener
{
public:
	typedef unsigned short			  port_type;
	typedef RakNet::RakPeerInterface* rak_peer_interface_ptr;
	typedef sf::Time				  secType;

	Client(port_type aPort);
	~Client();
	bool Connect(const char* aSeverAddress, port_type aServerPort) const;

	port_type getPort() { return mPort; }
	rak_peer_interface_ptr getPeer() { return mPeer; }
	void doNetworking(secType aDeltaT);
	void preUpdate();
	void postUpdate();

	// Mutators
	void setShapePosition(float x, float y) { mMyShape.setPosition(x, y); }

	// Events
	void handleEvent(const Event& theEvent);

	// help menu
	static void printUsage();
	static void startingGame(bool isStarting);
	
private:
	port_type			   mPort;
	rak_peer_interface_ptr mPeer;
	GamePacketHandler_I* mpPacketHandler;
	sf::RectangleShape mMyShape;
	std::vector<sf::RectangleShape> mOtherShapes;
	sf::Vector2f mMyShapeVelocity;
	sf::Vector2f mOtherShapeVelocity;
	std::vector<sf::RectangleShape> mBricks;
	sf::RectangleShape mBallShape;
	sf::Vector2f mBallDirection;
	bool mGameStarted;
};

#endif CLIENT_H