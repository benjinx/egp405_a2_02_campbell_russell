#include <iostream>
#include <memory>
/*

	THIS IS THE MAIN FOR SERVER CURRENTLY


*/
#include "Server.h"
#include "SFML/System/Clock.hpp"

int main(int argc, char** argv)
{
	typedef sf::Time secType;

	if (argc == 2)
	{
		const int portNum = atoi(argv[1]);
		std::unique_ptr<Server> pServer(new Server(portNum));
		pServer->launch();

		sf::Clock clock;

		while (1) // this should be like shouldexit
		{
			secType aDeltaT = clock.restart();

			pServer->preUpdate(aDeltaT);
		}
	}
	else
		Server::printUsage(); 
	

	// Initialize the game.
	/*Game::initInstance();
	if (!Game::getInstance()->init())
	{
		Game::cleanupInstance();
		system("pause");
		return EXIT_FAILURE;
	}

	bool serverOrClient;
	RakNet::RakPeerInterface* peer = RakNet::RakPeerInterface::GetInstance();

	if (argc == 2)
	{
		const int portNum = atoi(argv[1]);
		if (Services::getServer()->launch(peer, portNum) == false)
			return 0;
		serverOrClient = false;
	}
	else if (argc == 3 || argc == 4) // ServerIP serverport [clientPort]
	{
		const char* serverIP = argv[1];
		const int serverPort = atoi(argv[2]);
		const int clientPort = argc == 4 ? atoi(argv[3]) : serverPort + 1;

		if (Services::getNetworkingSystem()->launchClient(peer, serverIP, serverPort, clientPort) == false)
			return 0;
		serverOrClient = true;
	}
	else
	{
		Services::getNetworkingSystem()->printUsage();
		system("pause");
		return 0;
	}


	// Run the game loop.
	Game::getInstance()->gameLoop(peer);

	// Cleanup the game.
	Game::getInstance()->cleanup();
	Game::cleanupInstance();

	peer->Shutdown(300);
	RakNet::RakPeerInterface::DestroyInstance(peer);*/

	system("pause");
	return EXIT_SUCCESS;
}