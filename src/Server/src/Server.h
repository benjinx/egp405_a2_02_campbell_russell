#ifndef SERVER_H
#define SERVER_H

#include "../../common/src/PacketHandler.h"
#include "../../common/src/GamePacketHandler.h"
#include "SFML/System/Clock.hpp"
#include <memory>
#include <algorithm>
#include "RakPeer.h"

namespace RakNet { class RakPeerInterface; };

class PacketHandlerServer : public GamePacketHandler_I
{
public:
	PacketHandlerServer(rak_peer_ptr aPeer)
		: GamePacketHandler_I(aPeer)
	{ }

	bool HandleMessage(packet_id_type aID, rak_peer_ptr aPeer, packet_ptr aPacket)
	{
		switch (aID)
		{
		case ID_NEW_INCOMING_CONNECTION:
		{
			printf("\nNew incoming connection from %s w/ GUID %s", aPacket->systemAddress.ToString(true), aPacket->guid.ToString());
			
			NumOfClients noc;
			noc.mPacketID = packet_id::ID_NUM_OF_CLIENTS;
			noc.numOfPlayers = mPlayerInfo.size();
			noc.mGUID = aPeer->GetMyGUID();
			aPeer->Send((const char*)&noc, sizeof(NumOfClients), HIGH_PRIORITY, UNRELIABLE_SEQUENCED, 0, RakNet::UNASSIGNED_SYSTEM_ADDRESS, true);
			return true;
		}
		case ID_CONNECTION_LOST:
		case ID_CONNECTION_BANNED:
		case ID_DISCONNECTION_NOTIFICATION:
		{
			auto itr = std::find_if(mPlayerInfo.begin(), mPlayerInfo.end(), [aPacket](const PlayerInfo& aPI)
			{
				if (aPacket->guid == aPI.mGUID)
					return true;
				return false;
			});

			//if (itr != mPlayerInfo.end())
			//{
				printf("\nPlayer disconnected: ", itr->mGUID.ToString());
				mPlayerInfo.erase(itr);
			//}

			return true;
		}
		case packet_id::ID_PLAYER_STATE:
		{
			auto itr = std::find_if(mPlayerInfo.begin(), mPlayerInfo.end(), [aPacket](const PlayerInfo& aPI)
			{
				if (aPacket->guid == aPI.mGUID)
					return true;
				return false;
			});


			if (itr == mPlayerInfo.end())
			{
				PlayerInfo temp;
				temp.setGUID(aPacket->guid);
				mPlayerInfo.push_back(temp);

				itr = mPlayerInfo.end() - 1;
			}

			ShapeInfo si = *(ShapeInfo*)aPacket->data;
			itr->setState(si);

			// broadcast the state to everybody EXCEPT the client who sent us the
			// state
			aPeer->Send((const char*)&si, sizeof(ShapeInfo), HIGH_PRIORITY,
				UNRELIABLE_SEQUENCED, 0, aPacket->systemAddress, true);

			return true;
		}

		case packet_id::ID_START_GAME:
		{
			GameStarting gs = *(GameStarting*)aPacket->data;
			aPeer->Send((const char*)&gs, sizeof(GameStarting), HIGH_PRIORITY,
				UNRELIABLE_SEQUENCED, 0, aPacket->systemAddress, true);
			return true;
		}

		}

		return true;
	}
};

class Server
{
public:
	typedef unsigned short			  port_type;
	typedef RakNet::RakPeerInterface* rak_peer_interface_ptr;
	typedef sf::Time				  secType;

	Server(port_type aPort);
	~Server();
	bool launch() const;

	//void setPort(port_type aPort) { mPort = aPort; }
	//void setPeer(rak_peer_interface_ptr aPeer) { mPeer = aPeer; }
	port_type getPort() { return mPort; }
	rak_peer_interface_ptr getPeer() { return mPeer; }
	void preUpdate(secType aDeltaTime);
	static void printUsage();

private:
	port_type			   mPort;
	rak_peer_interface_ptr mPeer;
	PacketHandlerServer* mpPacketHandler;
};

#endif SERVER_H