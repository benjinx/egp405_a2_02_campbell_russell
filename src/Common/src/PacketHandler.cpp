#include "Packet.h"
#include "PacketHandler.h"
#include "MessageIdentifiers.h"
#include "RakPeerInterface.h"
#include <iostream>

PacketHandler_I::PacketHandler_I(rak_peer_ptr aPeer)
: mPeer(aPeer)
{
	//if (aPeer == NULL)
		
}

void PacketHandler_I::update(sec_type aDeltaTime)
{
	HandlePackets(aDeltaTime, mPeer);
}

void PacketHandler_I::Pre_HandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer)
{

}

void PacketHandler_I::HandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer)
{
	Pre_HandlePackets(aDeltaTime, aPeer);
	DoHandlePackets(aDeltaTime, aPeer);
	Post_HandlePackets(aDeltaTime, aPeer);
}

void PacketHandler_I::Post_HandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer)
{

}

void PacketHandler_I::DoHandlePackets(sec_type aDeltaTime, rak_peer_ptr aPeer)
{
	for (RakNet::Packet* p = aPeer->Receive(); p; aPeer->DeallocatePacket(p), p = aPeer->Receive())
	{
		auto packetID = f_packet::getPacketID(p);

		if (HandleMessage(packetID, aPeer, p)) { continue; }

		switch (packetID)
		{
			case ID_CONNECTION_LOST:
			{
				printf("\nConnection lost from %s", p->systemAddress.ToString(true));
				break;
			}

			default:
			{
				printf("\nReceived unhandled packet");
			}
		}
	}

	Post_HandlePackets(aDeltaTime, aPeer);
}