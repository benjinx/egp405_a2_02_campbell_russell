#ifndef GAME_PACKET_HANDLER_H
#define GAME_PACKET_HANDLER_H

#include "PacketHandler.h"
#include "PlayerInfo.h"
#include <vector>

class GamePacketHandler_I : public PacketHandler_I
{
public:
	typedef std::vector<PlayerInfo> playerInfoCont;

	GamePacketHandler_I(rak_peer_ptr aPeer);

	PlayerInfo getPlayerInfoAt(int location) { return mPlayerInfo.at(location); }
	std::vector<PlayerInfo> getPlayerInfo() { return mPlayerInfo; }

protected:
	playerInfoCont mPlayerInfo;
};

#endif GAME_PACKET_HANDLER_H