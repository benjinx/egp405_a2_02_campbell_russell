#ifndef PLAYER_INFO_H
#define PLAYER_INFO_H

#include "Packet.h"
#include "RakNetTypes.h"

struct PlayerInfo
{
	typedef PlayerInfo thisType;

	RakNet::RakNetGUID mGUID;
	//MousePos mState;
	ShapeInfo mState;
	sf::Vector2f mVelocity;

	void setGUID(RakNet::RakNetGUID aGUID) { mGUID = aGUID; }
	//void setState(MousePos aState) { mState = aState; }
	void setState(ShapeInfo aState) { mState = aState; }
};


#endif PLAYER_INFO_H