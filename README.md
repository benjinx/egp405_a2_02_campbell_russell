# README #

### Team Members ###
* Benjamin Campbell
* Benjamin Russell

### Repository Location ###
https://bitbucket.org/benjinx/egp405_a2_02_campbell_russell.git

### Game Description #s##

Multiplayer Pong Break.

In this variation of the classic Pong, two players face off in a game of dexterity and wits. Where normally lies an open goal there resides 24 defensive bricks each sworn the oath to your cause. They will gladly sacrifice their lives to deflect the ball back. Now! Lead your mighty brick army to victory over your opponent. Be wary. Allow the enemy's return shot through a gap in the front lines and you risk oblivion, or they might just gain a point. First to 5 wins.

### Controls ###

Up and Down - Move your mighty padel.

### Set Up ###

* The project will need to be assembled with CMake in 32bit.
* After creating the project, boot up the solution and compile both the client and the server.
* Once everything has been compiled as release or debug just run the .bat files and fill in the required information to each.
* (Server first then clients)